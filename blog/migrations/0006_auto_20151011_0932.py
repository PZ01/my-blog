# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_auto_20150916_0936'),
    ]

    operations = [
        migrations.CreateModel(
            name='BlogEntryImage',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('last_modified', models.DateTimeField(editable=False, default=datetime.datetime.now)),
                ('created', models.DateTimeField(editable=False, default=datetime.datetime.now)),
                ('title', models.CharField(max_length=20)),
                ('image', models.ImageField(height_field='height', upload_to='images/%Y/%m/%d', width_field='width')),
                ('width', models.IntegerField(editable=False)),
                ('height', models.IntegerField(editable=False)),
            ],
        ),
        migrations.AlterField(
            model_name='entry',
            name='feeling',
            field=models.CharField(choices=[('ANG', 'Anger'), ('GRA', 'Gratitude'), ('TRI', 'Triumph'), ('SRW', 'Sorrow'), ('PRD', 'Pride'), ('GLT', 'Guilt'), ('INF', 'Indifference'), ('DSR', 'Desire'), ('CRS', 'Curious'), ('AVS', 'Aversion'), ('HPE', 'Hope'), ('FER', 'Fear'), ('CRG', 'Courage'), ('EVY', 'Envy'), ('JOY', 'Joy'), ('BRM', 'Boredom'), ('HRT', 'Hurt'), ('RLF', 'Relief'), ('STS', 'Stress')], max_length=3, default='JOY'),
        ),
        migrations.AddField(
            model_name='blogentryimage',
            name='blog_entry',
            field=models.ForeignKey(to='blog.Entry'),
        ),
    ]
