# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0008_auto_20151011_1004'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='body_html',
            field=models.TextField(default='Hello'),
            preserve_default=False,
        ),
    ]
