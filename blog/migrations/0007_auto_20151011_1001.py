# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0006_auto_20151011_0932'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogentryimage',
            name='image',
            field=models.ImageField(width_field='width', height_field='height', upload_to='images/%Y/%m'),
        ),
    ]
