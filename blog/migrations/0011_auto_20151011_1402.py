# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0010_auto_20151011_1400'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='entry',
            name='feeling',
        ),
        migrations.RemoveField(
            model_name='entry',
            name='global_feeling',
        ),
    ]
