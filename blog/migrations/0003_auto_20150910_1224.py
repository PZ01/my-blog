# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_auto_20150904_0811'),
    ]

    operations = [
        migrations.CreateModel(
            name='StaticContent',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('page_name', models.CharField(max_length=100, unique=True)),
                ('body', models.TextField()),
            ],
        ),
        migrations.AlterField(
            model_name='entry',
            name='created',
            field=models.DateTimeField(default=datetime.datetime.now),
        ),
    ]
