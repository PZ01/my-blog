# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0009_entry_body_html'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entryimage',
            name='title',
            field=models.CharField(max_length=50),
        ),
    ]
