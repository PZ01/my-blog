# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0007_auto_20151011_1001'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='BlogEntryImage',
            new_name='EntryImage',
        ),
    ]
