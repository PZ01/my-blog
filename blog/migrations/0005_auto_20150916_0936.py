# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_thought'),
    ]

    operations = [
        migrations.AddField(
            model_name='entry',
            name='feeling',
            field=models.CharField(max_length=3, default='JOY', choices=[('ANG', 'Anger'), ('GRA', 'Gratitude'), ('TRI', 'Triumph'), ('SRW', 'Sorrow'), ('PRD', 'Pride'), ('GLT', 'Guilt'), ('INF', 'Indifference'), ('DSR', 'Desire'), ('CRS', 'Curious'), ('AVS', 'Aversion'), ('HPE', 'Hope'), ('FER', 'Fear'), ('CRG', 'Courage'), ('EVY', 'EVY'), ('JOY', 'JOY'), ('BRM', 'BRM'), ('HRT', 'HRT'), ('RLF', 'RLF'), ('STS', 'STS')]),
        ),
        migrations.AddField(
            model_name='entry',
            name='global_feeling',
            field=models.CharField(max_length=3, default='POS', choices=[('POS', 'POS'), ('NEG', 'NEG')]),
        ),
    ]
