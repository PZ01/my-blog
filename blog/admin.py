from django.contrib import admin
from . import models
from django_markdown.admin import MarkdownModelAdmin
from django_markdown.widgets import AdminMarkdownWidget
from django.db.models import TextField

class StaticContentAdmin(admin.ModelAdmin):
    list_display = ("page_name",)
    formfield_overrides = {TextField: {'widget': AdminMarkdownWidget}}

class ThoughtAdmin(admin.ModelAdmin):
    list_display = ("body",)

class EntryImageInline(admin.TabularInline):
    model = models.EntryImage

class EntryAdmin(admin.ModelAdmin):
    list_display = ("title", "created", "publish")
    formfield_overrides = {TextField: {'widget': AdminMarkdownWidget}}
    readonly_fields = ("modified",)
    fields = ("title", "slug", "created", "modified", "body", "tags", "publish",)
    prepopulated_fields = {"slug": ("title",)}
    inlines = [EntryImageInline,]

admin.site.register(models.Entry, EntryAdmin)
admin.site.register(models.StaticContent, StaticContentAdmin)
admin.site.register(models.Tag)
admin.site.register(models.Thought, ThoughtAdmin)
