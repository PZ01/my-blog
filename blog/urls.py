from django.conf.urls import patterns, include, url
from django.conf import settings
from . import views, feed

urlpatterns = patterns(
    '',
    url(r'^$', views.BlogIndex.as_view(), name="index"),
    url(r'^feed/$', feed.LatestPosts(), name="feed"),
    url(r'^entry/(?P<slug>\S+)$', views.BlogDetail.as_view(), name="entry_detail"),
    url(r'^archive/$', views.BlogArchive.as_view(), name="archive"),
    url(r'^archive/(?P<year>[0-9]{4})/$', views.BlogArchiveYear.as_view(), name="archive"),
    url(r'^archive/(?P<year>[0-9]{4})/(?P<month>[0-9]{2})$', views.BlogArchiveYearMonth.as_view(), name="archive"),
    url(r'^tag/(?P<slug>\S+)/$', views.BlogEntriesInTagList.as_view(), name="tag"),
    url(r'^about/$', views.BlogAbout.as_view(), name="about"),
    url(r'^specs/$', views.BlogSpecs.as_view(), name="specs"),
)

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}))
