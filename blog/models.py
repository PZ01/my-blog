from __future__ import unicode_literals

from django.db import models
from django.core.urlresolvers import reverse
from django.db.models.aggregates import Count

from datetime import datetime  
from random import randint

import re
import os

class Tag(models.Model):
    slug = models.SlugField(max_length=200, unique=True)

    def __str__(self):
        return self.slug

class StaticContent(models.Model):
    page_name = models.CharField(max_length=100, blank=False, unique=True)
    body = models.TextField()

class ThoughtManager(models.Manager):
    def random(self):
        count = self.aggregate(count=Count('id'))['count']
        random_index = randint(0, count - 1)
        return self.all()[random_index]

class Thought(models.Model):
    body = models.CharField(max_length=200, blank=False, unique=True)
    objects = ThoughtManager()

class EntryQuerySet(models.QuerySet):
    def published(self):
        return self.filter(publish=True)

    def in_year(self, year):
        return self.filter(created__year=year)

    def in_year_month(self, year, month):
        return self.filter(created__year=year, created__month=month)

    def tagged_with(self, tag_name):
        return self.filter(tags__slug=tag_name)

class Entry(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField()
    body_html = models.TextField()
    slug = models.SlugField(max_length=200, unique=True)
    publish = models.BooleanField(default=False)
    created = models.DateTimeField(default=datetime.now)
    modified = models.DateTimeField(auto_now=True)
    tags = models.ManyToManyField(Tag)

    objects = EntryQuerySet.as_manager()

    def get_absolute_url(self):
        return reverse("entry_detail", kwargs={"slug": self.slug})

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        def build_img(imgUrl, imgTitle):
            #images/2015/10/Golden_Gate_Bridge_thumbnail.jpg
            imgUrlBase = os.path.dirname(imgUrl)
            imgNameWithExt = imgUrl.split('/')[-1] 
            imgName = imgNameWithExt.split('.')[0]
            imgThumbWithExt = imgName + "_thumbnail" + "." + imgNameWithExt.split('.')[1]

            return ("<div class='thumbnail'>"
                    "<a href='" + imgUrl + "'>"
                    "<img alt='" + imgNameWithExt + "' "
                    "src='" + imgUrlBase + '/' + imgThumbWithExt + "' class='portrait'>"
                    "</a>"
                    "<div class='thumb_title'>" + imgTitle + "</div>"
                    "</div>"
                    )

        self.body_html = self.body
        for imageEntry in self.entryimage_set.all():
            imageTag = '![%s]' % imageEntry.title
            imageReplaceTag = build_img(imageEntry.image.url, imageEntry.title)
            print(imageTag)
            self.body_html = self.body_html.replace(imageTag, imageReplaceTag)
            print(self.body_html)

        super(Entry, self).save(*args, **kwargs) # Call the "real" save() method.

    class Meta:
        verbose_name = "Blog Entry"
        verbose_name_plural = "Blog Entries"
        ordering = ["-created"]

class EntryImage(models.Model):
    """
    An image associated with a blog entry
    """       
    last_modified = models.DateTimeField(default=datetime.now,editable=False)
    created = models.DateTimeField(default=datetime.now,editable=False)
    title = models.CharField(max_length=50)
    image = models.ImageField(upload_to='images/%Y/%m', height_field='height', width_field='width')
    width = models.IntegerField(editable=False)
    height = models.IntegerField(editable=False)
    blog_entry = models.ForeignKey(Entry)

