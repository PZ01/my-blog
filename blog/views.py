from django.views import generic
from . import models
import logging
import calendar

def create_archive_set():
        published_entries = models.Entry.objects.published().filter().order_by("created")
        earliest_year = published_entries[0].created.year
        latest_year = published_entries[len(published_entries) - 1].created.year
        
        # Create a dictionary with [year][month] keys mapping to an array of entries.
        entry_dict = {}
        for year in range(latest_year, earliest_year - 1, -1):
            entry_dict[year] = {}

            for month in range(1, 13):
                entry_dict[year][month] = []

        for entry in published_entries:
            entry_dict[entry.created.year][entry.created.month].append(entry)

        # Sort the dictionary
        entry_sorted_keys = list(reversed(sorted(entry_dict.keys())))
        list_entries = []

        for key in entry_sorted_keys:
            list_entries.append({key:entry_dict[key]}) 

        return list_entries

class BlogIndex(generic.ListView):
    queryset = models.Entry.objects.published()
    template_name = "home.html"
    paginate_by = 8

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(BlogIndex, self).get_context_data(**kwargs)
        context['entry_archive_list'] = create_archive_set()
        context['page_name'] = self.template_name.split(".")[0]
        context['random_thought'] = models.Thought.objects.random().body

        return context

class BlogDetail(generic.DetailView):
    model = models.Entry
    template_name = "post.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(BlogDetail, self).get_context_data(**kwargs)
        context['random_thought'] = models.Thought.objects.random().body

        return context

class BlogArchive(generic.TemplateView):
    queryset = models.Entry.objects.published()
    template_name = "archive.html"
    
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(BlogArchive, self).get_context_data(**kwargs)
        context['entry_archive_list'] = create_archive_set()
        context['page_name'] = self.template_name.split(".")[0]
        context['random_thought'] = models.Thought.objects.random().body

        return context

class BlogArchiveYear(generic.TemplateView):
    template_name = "archive_by_year.html"
    
    def entries_by_months(self, year):
        entries_in_year = models.Entry.objects.in_year(year)
        entries_by_months = {}

        for month in range(1, 13):
            entries_by_months[month] = []

        for entry in entries_in_year:
            entries_by_months[entry.created.month].append(entry)

        entries_by_months = {k:v for k,v in entries_by_months.items() if v}
        
        return entries_by_months


    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(BlogArchiveYear, self).get_context_data(**kwargs)
        context['page_name'] = self.template_name.split("_")[0]
        context['entry_archive_list'] = create_archive_set()
        context['entries_by_months'] = self.entries_by_months(kwargs['year'])
        context['year'] = kwargs['year']
        context['random_thought'] = models.Thought.objects.random().body
    
        return context

class BlogArchiveYearMonth(generic.TemplateView):
    template_name = "archive_by_yearmonth.html"
    
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(BlogArchiveYearMonth, self).get_context_data(**kwargs)
        context['page_name'] = self.template_name.split("_")[0]
        context['entry_archive_list'] = create_archive_set()
        context['entries_inyearmonth'] = models.Entry.objects.in_year_month(kwargs['year'], kwargs['month'])
        context['year'] = kwargs['year']
        context['random_thought'] = models.Thought.objects.random().body

        try:
            context['month'] = calendar.month_name[int(kwargs['month'])]
        except IndexError:
            context['month'] = calendar.month_name[0]

        return context

class BlogEntriesInTagList(generic.ListView):
    template_name = "entriesbytag.html"

    def get_queryset(self):
        return models.Entry.objects.tagged_with(self.kwargs['slug'])

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(BlogEntriesInTagList, self).get_context_data(**kwargs)
        context['slug'] = self.kwargs['slug']
        context['random_thought'] = models.Thought.objects.random().body

        return context

class BlogAbout(generic.TemplateView):
    template_name = "about.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(BlogAbout, self).get_context_data(**kwargs)
        context['page_name'] = self.template_name.split(".")[0]
        context['about_content'] = models.StaticContent.objects.get(page_name="about")
        context['random_thought'] = models.Thought.objects.random().body

        return context

class BlogSpecs(generic.TemplateView):
    template_name = "specs.html"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(BlogSpecs, self).get_context_data(**kwargs)
        context['specs_content'] = models.StaticContent.objects.get(page_name="specs")
        context['random_thought'] = models.Thought.objects.random().body

        return context

