# README #

## Running the server locally

```
#!bash

#local settings
python3 manage.py runserver --settings=mysite.settings.local

#production settings
python3 manage.py runserver --settings=mysite.settings.production --insecure
```

## Deploying static files

```
#!bash

python3 manage.py collectstatic --settings=mysite.settings.production
```

## Deploying the blog on pythonanywhere

Things to remember :

ALLOWED_HOSTS has to be set to the domain, otherwise a 400 BAD REQUEST will show up on the page.

Steps : 
1. $cd ./my-site && git pull
2. Enter RSA password
3. $'workon django17'
4. Run collectstatic


## List of features to implement
1. Add Disqus to individual posts
2. Add a newsletter
3. Google AdSense
4. Continue Importing Entries

## Other notes

To add images to a blog article currently, you edit an entry in the admin panel and use the following syntax:
```
#!python

![Title]
```
The 'Title' is the same one as the 'Title' in the image chooser of the Entry panel(at the bottom of the page).

An image called imgname_thumbnail.jpg must also exist with a size of 250x250. A resize script exists in ~/scripts/resize.sh
