from mysite.settings.base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# A list of strings representing the host/domain names that this Django site can serve. 
# This is a security measure to prevent an attacker from poisoning caches and triggering
# password reset emails with links to malicious hosts by submitting requests with a fake HTTP Host header, 
# which is possible even under many seemingly-safe web server configurations.
ALLOWED_HOSTS = ['www.coaltober.com', 'coaltober.com', '127.0.0.1']

# Dynamic file storage settings
#######################################

# Absolute filesystem path to the directory that will hold user-uploaded files.
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# URL that handles the media served from MEDIA_ROOT, used for managing stored files.
MEDIA_URL = '/media/'

# STATIC files and COMPRESSION settings
#######################################

# The absolute path to the directory where collectstatic will collect static files for deployment.
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

# URL to use when referring to static files located in STATIC_ROOT.
STATIC_URL = '/static/'

# The list of finder backends that know how to find static files in various locations.
STATICFILES_FINDERS += (
    'compressor.finders.CompressorFinder',
)

# Enable static file compression (default = !DEBUG)
COMPRESS_ENABLED = True 

COMPRESS_PRECOMPILERS = (
    ('text/x-scss', 'django_libsass.SassCompiler'),
)
